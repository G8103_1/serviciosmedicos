const jwt =require ('jsonwebtoken');
const bcrypt =require ("bcrypt");
const {model}= require ('mongoose');
const User =require('../models/UserModels');
 
async function login (req, res){

    const user= await User.findOne({
        user: req.body.user
    });
     if(user==null){
        res.status(403).send({error: "Invalid credentials"});
        return;
     }else{
            const validPassword =await bcrypt.compare(req.body.password, user.password);
             if(!validPassword){
              res.status(400).json({error: "Invalid Password"});
              return;
            }
        let token=await  new Promise ((resolve,reject)=>{
            jwt.sign(user.toJSON(), 'secretKey',{expiresIn:'1h'}, (err,token)=>{
                if(err){
                    reject(err);
                }else{
                    resolve(token);
                }
            });
        });
        res.status(200).send({token:token});
        return;
       }
    }

 function test(req, res){
 res.status(200).send({testResult: req.data});
 }

 function verifyToken(req, res, next){
    const requestHeader= req.headers['authorization'];
    
      if(typeof requestHeader !=='undefined'){
        const token= requestHeader.split(" ")[1];
        jwt.verify(token,'secretKey', (err, data)=>{
            if(err){
                res.sendStatus(403);
            }else{
                req.data=data;
                next();
            }
        });
        }else{
        res.sendStatus(403);
          }
 }

 module.exports={login, test, verifyToken};