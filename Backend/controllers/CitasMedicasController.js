const { default: mongoose } = require('mongoose');
let citasMedicas = require('../Models/ServiciosMedicosModels');

function savecitasMedicas(req, res){
    let citasMedicas1 =new citasMedicas(req.body);
    citasMedicas1.save((err, result)=>{
        if(err){
            res.status(500).send({message: "Error creación usuario. Error 500"});
        }else{
            res.status(200).send({message: result});
        }
    });
}

function buscarData(req,res){
    var idcitasMedicas=req.params.id;
    citasMedicas.findById(idcitasMedicas).exec((err,result)=>{
        if(err){
            res.status(500).send({message:'Error al momento de ejecutar la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro a buscar no se encuentra disponible'});
            }else{
                res.status(200).send({result});
            }
        }
    });
}

function listarAllData(req,res){
    var idcitasMedicas=req.params.idb;
    if(!idcitasMedicas){
        var result=citasMedicas.find({}).sort('user');
    }else{
        var result=citasMedicas.find({_id:idcitasMedicas}).sort('user');
    }
    result.exec(function(err,result){
        if(err){
            res.status(500).send({message:'Error al momento de ejecutar la solicitud'});
        }else{
            if(!result){
                res.status(404).send({message:'El registro a buscar no se encuentra disponible'});
            }else{
                res.status(200).send({result});
            }
        }
    });
}

function updatecitasMedicas(req,res){
    let id=req.params.id;
    let objectId= mongoose.Types.ObjectId(id);
    let data=req.body;
    citasMedicas.findByIdAndUpdate({_id: objectId}, data, {new: true}, (err, result)=>{
        if (err){
            res.status(500).send({message:'No se pudo actualizar'});
        }else{
            res.status(200).send(result);
        }
    });
}

function deletecitasMedicas(req,res){
    let id=req.params.id;
    citasMedicas.findByIdAndDelete(id, (err, result)=>{
        if (err){
            res.status(500).send({message:'No se pudo eliminar'});
        }else{
            res.status(200).send(result);
        }
    });
}


module.exports= { savecitasMedicas, buscarData, listarAllData,
                    updatecitasMedicas,deletecitasMedicas };