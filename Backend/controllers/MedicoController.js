const { get, send } = require("express/lib/response");
const Post = require("../models/MedicoModels");

function saveMedico (req, res) {
    let myPost = new Post(req.body);

    myPost.save((err, result) => {
        if(err){
            res.status(500).send({message : "Post creation failed. Error 500"})
        }else {
            res.status(200).send({message: result})
        }
    });
}

function readMedico(req, res){
    let query = Post.find({}).sort('created');

    query.exec(function(err, result){
        if(err){
            res.status(500).send({message : err});
        }else{
            res.status(200).send(result);
        }
    });
}

function readID(req, res){
    let Id = req.params.id;
    let query = Post.findById(Id).sort('content');

    query.exec(function(err, result){
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send(result);
        }
    });
}

function updateMedico( req, res ){

    let id = req.params.id;
    let data = req.body;

    Post.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteMedico( req, res ){

    let id = req.params.id;

    Post.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}

module.exports = {saveMedico, readMedico, readID, updateMedico, deleteMedico };