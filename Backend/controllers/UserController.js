const { get, send } = require("express/lib/response");
const Post = require("../models/UserModels");
const bcrypt= require("bcrypt");

async function savePost (req, res) {
    let myPost = new Post(req.body);
    const salt= await bcrypt.genSalt(10);
    myPost.password = await bcrypt.hash(myPost.password, salt);
    
    myPost.save((err, result) => {
        if(err){
            res.status(500).send({message : "Post creation failed. Error 500"})
        }else {
            res.status(200).send({message: result})
        }
    });
}

function readUser(req, res){
    let query = Post.find({}).sort('created');

    query.exec(function(err, result){
        if(err){
            res.status(500).send({message : err});
        }else{
            res.status(200).send(result);
        }
    });
}

function readID(req, res){
    let Id = req.params.id;
    let query = Post.findById(Id).sort('content');

    query.exec(function(err, result){
        if(err){
            res.status(500).send({message: err});
        }else{
            res.status(200).send(result);
        }
    });
}


function updateUsuario( req, res ){

    let id = req.params.id;
    let data = req.body;

    Post.findByIdAndUpdate( id, data, { new: true }, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );

}

function deleteUsuario( req, res ){

    let id = req.params.id;

    Post.findByIdAndDelete( id, (err, result) => {
        if(err){
            res.status(500).send( {message: err });
        }else{
            res.status(200).send( result );
        }
    } );
    
}


module.exports = { savePost, readUser, readID, updateUsuario, deleteUsuario};