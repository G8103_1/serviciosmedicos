const res = require('express/lib/response');
const mongoose = require('mongoose');

mongoose.connect("mongodb://localhost:27017/db_mintic", { }, (err, res)=>{
    if(err){
        console.log("Error connecting to database");
    }else{
        console.log("connection successful");
    }
});

module.exports = mongoose;
