const { Router } = require ('express');
const UserController = require ('../controllers/UserController');
const MedicoController = require ('../controllers/MedicoController');
const citasMedicasController = require('../controllers/CitasMedicasController')
const authController = require('../controllers/authController');


let router = Router();
// endpoint User
router.post('/user/save', authController.verifyToken, UserController.savePost);
router.put('/user/update/:id', authController.verifyToken, UserController.updateUsuario);
router.delete('/user/delete/:id',authController.verifyToken, UserController.deleteUsuario);
router.get('/user/list', UserController.readUser);
router.get('/user/:id', UserController.readID);

// endpoint Medico
router.post('/Medico/post/save', MedicoController.saveMedico);
router.put('/Medico/put/:id', MedicoController.updateMedico);
router.delete('/Medico/delete/:id',MedicoController.deleteMedico );
router.get('/Medico/get/readMedico', MedicoController.readMedico);
router.get('/Medico/get/:id', MedicoController.readID)


// endpoint  servicios medicos

router.post('/citas/post/save', citasMedicasController.savecitasMedicas);
router.put('/citas/put/:id',citasMedicasController.updatecitasMedicas);
router.delete('/citas/delete/:id',citasMedicasController.deletecitasMedicas);
router.get('/citas/get/:id',citasMedicasController.buscarData);
router.get('/citas/get/:id?',citasMedicasController.listarAllData);

// Auth
router.post('/auth/login',authController.login);
router.post('/auth/test', authController.verifyToken, authController.test);

module.exports = router;
