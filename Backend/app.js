const express = require('express');
const bodyParser =require('body-parser');
const router =require('./routers/router');
const req = require('express/lib/request');
const res = require('express/lib/response');

let app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Header', 'Authorization, X-API-KEY, Origin, XRequested-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');
    next();
});

app.use(router);
module.exports = app;