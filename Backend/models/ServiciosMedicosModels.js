const mongoose= require('mongoose');

let Schema =mongoose.Schema;

const CitasMedicas = Schema({
    user: {type:String, required: true},
    Medico: {type:String, required: true},
    Fecha: {type:String, required: true},
    Hora: {type:String, required: true},
    Lugar: {type:String, required: true},
    Estado: {type:String, required: true},
    TipodeServicio: {type:String, required: true},
    created: {type: Date, default: Date.now}
});

const citasMedicas= mongoose.model('CitasMedicas', CitasMedicas);

module.exports = citasMedicas;