const mongoose = require('mongoose');

let Schema = mongoose.Schema;

const Medico = Schema({
    name: { type: String, require:true },
    last_name: {type: String, require:true },
    document: {type: Number, require:true },
    Email: { type:String, require:true },
    phone:{ type:Number, require:true }
});


const MedicoM = mongoose.model('Medico', Medico);

module.exports = MedicoM;