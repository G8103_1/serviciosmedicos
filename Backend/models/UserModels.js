const mongoose =  require('mongoose');

let Schema = mongoose.Schema;

const Usuario = Schema({
    name: { type: String, require:true },
    last_name: {type: String, require:true },
    document: {type: Number, require:true },
    Email: { type:String, require:true },
    phone:{ type:Number, require:true },
    user: { type: String, require:true },
    password : {type:String, require:true },
    role : {type:String, require:true }
},
{
    timestamps: true
});


const UsuarioM = mongoose.model('User', Usuario);

module.exports = UsuarioM;
