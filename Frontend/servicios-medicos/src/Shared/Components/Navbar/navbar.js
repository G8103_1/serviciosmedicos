import './navbar.scss'
export default function Navbar() {
    return (
        <div className="p-3">
            <nav className="navbar navbar-expand-lg navbar-light bg-light container-nav">
            
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav mr-auto">
                    <li className="nav-item active">
                        <a className="nav-link" href="./home">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a className="nav-link" href="./citas">Citas</a>
                    </li>
                    <li class="nav-item">
                        <a className="nav-link" href="./historial">Historico</a>
                    </li>
                </ul>
            </div>
            </nav>
        </div>
    )
}