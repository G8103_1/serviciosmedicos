import Navbar from "../../../Shared/Components/Navbar/navbar"
import './../Scss/Historico.scss';

export default function Historico() {
    return (
        <div className="container-medicos">
            <Navbar />
            <div className="row m-0">
                <div className="col-lg-12">
                    <div className="background-table p-3">
                        <span>Historia de citas medicas</span>
                        <table class="table mt-3">
                            <thead>
                                <tr>
                                <th scope="col">Número cita</th>
                                <th scope="col">Fecha cita</th>
                                <th scope="col">Profesional atendio</th>
                                <th scope="col">Especialidad</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <th scope="row">1</th>
                                <td>Mark</td>
                                <td>Otto</td>
                                <td>@mdo</td>
                                </tr>
                                <tr>
                                <th scope="row">2</th>
                                <td>Jacob</td>
                                <td>Thornton</td>
                                <td>@fat</td>
                                </tr>
                                <tr>
                                <th scope="row">3</th>
                                <td>Larry</td>
                                <td>the Bird</td>
                                <td>@twitter</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    )
}