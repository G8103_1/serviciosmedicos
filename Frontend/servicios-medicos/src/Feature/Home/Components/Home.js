import Navbar from "../../../Shared/Components/Navbar/navbar";
import './../Scss/Home.scss';
import fondoHome from './../../../Assets/img/fondo-home.jpg';
import fondoHome2 from './../../../Assets/img/fondo-home2.jpg';

export default function Home() {
    return (
        <div className="container-medicos">
            <Navbar />
            <div className="row m-0">
                <div className="col-sm-12 col-md-12 col-lg-6">
                    <div className=" container-home">
                        <span>Ultimas noticias</span>
                        <img src={fondoHome}  height={300}/>
                    </div> 
                </div>
                <div className="col-sm-12 col-md-12 col-lg-6">  
                    <div className=" container-home">
                        <span>Servicios medicos</span>
                        <img src={fondoHome2}  height={300}/>
                    </div> 
                </div>
            </div>
        </div>
    )
}