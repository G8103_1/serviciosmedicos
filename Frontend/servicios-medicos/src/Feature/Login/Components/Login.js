import './../Scss/login.scss';

export default function Login () {
    return (
        <div className='background-login'>
            <div className='container-login'>
                <div className='row'>
                    <div className='col-12'>
                        <span className='text-white'>Usuario</span>
                        <input className='form-control'/>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12'>
                        <span className='text-white'>Contraseña</span>
                        <input className='form-control' type='password'/>
                    </div>
                </div>
                <div className='row mt-3'>
                    <div className='col-12 d-flex justify-content-end'>
                        <button type="button" className="btn btn-secondary d-flex justifi">Iniciar sesión</button>
                    </div>
                </div>
            </div>
        </div>
    )
}