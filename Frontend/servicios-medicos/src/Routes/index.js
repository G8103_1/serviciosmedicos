import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Citas from '../Feature/Citas/Components/Citas';
import Historico from '../Feature/Historico/Components/Historico';
import Home from '../Feature/Home/Components/Home';
import Login from '../Feature/Login/Components/Login';

export default function Router() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path='/login' element={<Login />}></Route>
                <Route path='/citas' element={<Citas />}></Route>
                <Route path='/home' element={<Home />}></Route>
                <Route path='/historial' element={<Historico />}></Route>
            </Routes>
        </BrowserRouter>
    )
}