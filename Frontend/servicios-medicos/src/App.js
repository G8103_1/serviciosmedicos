import 'bootstrap/dist/css/bootstrap.min.css';
import { provider } from 'react-redux';
import logo from './logo.svg';
import './App.css';
import Router from './Routes';
import { store } from './Shared/Services/store';

function App() {
  return (
    <div>
      <provider store={store}>
        <Router />
      </provider>
    </div>
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn Reactppp
    //     </a>
    //   </header>
    //   <div className="row">
    //     <div className="col-md-6">Ssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss</div>
    //     <div className="col-md-6">Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa</div>
    //   </div>
    // </div>
  );
}

export default App;
